# Chimera IoT BLE Stack #

This repository provides BLE stack wrapper implementation. It allows to develop BLE device configurations 

### Usage ###

1. Add maven repository url to project configuration.

        repositories {
                maven { url "http://dl.bintray.com/chimeraiot/maven" }
        }


2. Add next row to dependences section of project:

        compile ('com.chimeraiot.android:chimera-ble:0.1.5') {
            exclude group: 'com.android.support', module: 'support-annotations'
            exclude group: 'com.android.support', module: 'appcompat-v7'
        }

### License ###

```
Copyright (c) 2015 Chimera IoT
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```