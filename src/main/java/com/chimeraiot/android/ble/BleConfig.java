package com.chimeraiot.android.ble;

/**  */
public final class BleConfig {

    /** Indicates where debugging is enabled. */
    private static boolean isDebugEnabled = BuildConfig.DEBUG_BLE;

    private BleConfig() {
        // prevent instantiation
    }

    public static boolean isDebugEnabled() {
        return isDebugEnabled;
    }

    public static void setDebugEnabled(final boolean isDebugEnabled) {
        BleConfig.isDebugEnabled = isDebugEnabled;
    }

}
