/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble;

import com.chimeraiot.android.ble.sensor.Sensor;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothProfile;
import android.os.Bundle;
import android.util.Log;

import java.util.LinkedList;

/** BLE messages processor. Handles order of execution BLE actions. */
public class BleGattExecutor extends BluetoothGattCallback {
    /** Logging tag. */
    private static final String TAG = BleGattExecutor.class.getSimpleName();

    /** Queue of service actions. */
    private final LinkedList<ServiceAction> queue = new LinkedList<>();
    /** Current service action. */
    private volatile ServiceAction currentAction;

    /** Add read service action to queue. */
    public void read(final Sensor sensor, final String uuid) {
        add(sensor.read(uuid));
    }

    /** Add write service action to queue. */
    public void update(final Sensor sensor, final String uuid, Bundle bundle) {
        for (ServiceAction action : sensor.update(uuid, bundle)) {
            add(action);
        }
    }

    /** Add notify service action to queue. */
    public void notify(final Sensor sensor, final String uuid, boolean start) {
        add(sensor.notify(uuid, start));
    }

    /** Executes next service action from queue. */
    public synchronized void execute(BluetoothGatt gatt) {
        if (currentAction != null) {
            return;
        }

        while (!queue.isEmpty()) {
            final ServiceAction action = queue.pop();
            currentAction = action;
            if (BleConfig.isDebugEnabled()) {
                Log.d(TAG, "- execute: " + currentAction);
            }
            if (!action.execute(gatt)) {
                break;
            }

            currentAction = null;
        }
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt,
                                  BluetoothGattDescriptor descriptor,
                                  int status) {
        super.onDescriptorWrite(gatt, descriptor, status);
        if (waitForWrite()) {
            return;
        }

        checkAndRetry(status);
        execute(gatt);
    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt,
                                      BluetoothGattCharacteristic characteristic,
                                      int status) {
        super.onCharacteristicWrite(gatt, characteristic, status);

        checkAndRetry(status);
        execute(gatt);
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt,
                                     BluetoothGattCharacteristic characteristic,
                                     int status) {
        super.onCharacteristicRead(gatt, characteristic, status);
        if (waitForWrite()) {
            return;
        }

        checkAndRetry(status);
        execute(gatt);
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        super.onConnectionStateChange(gatt, status, newState);

        if (newState == BluetoothProfile.STATE_DISCONNECTED) {
            checkAndRetry(BluetoothGatt.GATT_FAILURE);
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        super.onServicesDiscovered(gatt, status);
        execute(gatt);
    }

    /** Adds service action into queue. */
    private synchronized void add(ServiceAction action) {
        if (!action.equals(currentAction) && !queue.contains(action)) {
            queue.add(action);
            if (BleConfig.isDebugEnabled()) {
                Log.d(TAG, "- add action: " + action.toString());
            }
        }
    }

    /** Checks whether action should be retied. */
    private synchronized void checkAndRetry(int status) {
        if (currentAction == null) {
            return;
        }

        if (status != BluetoothGatt.GATT_SUCCESS) {
            queue.addFirst(currentAction);
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "action failed: " + currentAction);
            }
        }
        currentAction = null;
    }

    /** Wait for onCharacteristicWrite for write action before execution of any other actions. */
    private synchronized boolean waitForWrite() {
        return currentAction != null && currentAction.getType() == ServiceAction.ActionType.WRITE;
    }

    /** BLE service action. */
    public abstract static class ServiceAction {
        /** Action type. */
        public enum ActionType {
            /** Not defined action. */
            NONE,
            /** BLE read action. */
            READ,
            /** BLE notify action. */
            NOTIFY,
            /** BLE write action. */
            WRITE
        }

        /** Void service action. */
        public static final ServiceAction VOID = new ServiceAction("", ActionType.NONE) {
            @Override
            public boolean execute(BluetoothGatt gatt) {
                // it is null action. do nothing.
                return true;
            }
        };

        /** Service characteristic UUID. */
        private final String uuid;
        /** Action type. */
        private final ActionType type;

        public ServiceAction(String uuid, ActionType type) {
            this.type = type;
            this.uuid = uuid;
        }

        public ActionType getType() {
            return type;
        }

        public String getUuid() {
            return uuid;
        }

        /***
         * Executes action.
         * @param gatt - bluetooth provider.
         * @return true - if action was executed instantly. false if action is waiting for
         *         feedback.
         */
        public abstract boolean execute(BluetoothGatt gatt);

        @Override
        public boolean equals(Object o) {
            if (o == null) {
                return false;
            }
            if (o == this) {
                return true;
            }
            if (!o.getClass().equals(getClass())) {
                return false;
            }

            ServiceAction v = (ServiceAction) o;
            return v.uuid.equals(uuid) && v.type == type;
        }

        @Override
        public int hashCode() {
            return uuid.hashCode();
        }

        @Override
        public String toString() {
            return uuid + "   " + type.name();
        }
    }
}
