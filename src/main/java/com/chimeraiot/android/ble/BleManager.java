/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble;

import com.chimeraiot.android.ble.sensor.DeviceDef;
import com.chimeraiot.android.ble.sensor.DeviceDefCollection;
import com.chimeraiot.android.ble.sensor.Sensor;
import com.chimeraiot.android.ble.utils.WeakSetUtils;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/** BLE manager. */
public class BleManager implements BleExecutorListener {
    /** Logging tag. */
    private static final String TAG = BleManager.class.getSimpleName();

    /** Disconnected from device. */
    public static final int STATE_DISCONNECTED = 0;
    /** Connecting to BLE device. */
    public static final int STATE_CONNECTING = 1;
    /** Connected to device. */
    public static final int STATE_CONNECTED = 2;
    /** Ready to execute BLE actions. */
    public static final int STATE_READY = 3;

    /** Max reconnection tries. Used to try connect few times more if connection failed. */
    private static final int MAX_RECONNECTION_TRIES = 5;

    /** Compatable devices. */
    private final DeviceDefCollection devices;
    /** BLE executor instance. */
    private final BleGattExecutor executor = BleUtils.createExecutor(this);
    /** BLE adapter. */
    private BluetoothAdapter adapter;
    /** bluetooth provider. */
    private Map<String, BluetoothGatt> gattMap = new HashMap<>();
    /** Reconnection tries performed. */
    private Map<String, Integer> reconnectionTries = new HashMap<>();

    /** Connected device addresses. */
    private final Set<String> connectedDevices = new HashSet<>();
    /** Connection state. */
    private int connectionState = STATE_DISCONNECTED;

    /** BLE services listener. Monster collection :/ */
    private final Set<WeakReference<BleServiceListener>> serviceListeners =
            WeakSetUtils.newWeakSet();

    public BleManager(DeviceDefCollection devices) {
        this.devices = devices;
    }

    public DeviceDefCollection getDeviceDefCollection() {
        return devices;
    }

    /**
     * Adds device events listener weakly.
     * @param listener - listener.
     */
    public void registerListener(BleServiceListener listener) {
        synchronized (serviceListeners) {
            WeakSetUtils.add(serviceListeners, listener);
        }
    }

    /**
     * Removes device events listener.
     * @param listener - listener.
     */
    public void unregisterListener(BleServiceListener listener) {
        synchronized (serviceListeners) {
            WeakSetUtils.remove(serviceListeners, listener);
        }
    }

    public List<BluetoothDevice> getConnectedDevices() {
        final List<BluetoothDevice> result = new ArrayList<>(gattMap.size());
        for (BluetoothGatt gatt : gattMap.values()) {
            result.add(gatt.getDevice());
        }
        return result;
    }

    public int getConnectionState() {
        return connectionState;
    }

    public boolean isReady() {
        return connectionState == STATE_READY;
    }

    /**
     * Initializes a reference to the local Bluetooth adapter.
     *
     * @return Return true if the initialization is successful.
     */
    public boolean initialize(Context context) {
        if (adapter == null) {
            adapter = BleUtils.getBluetoothAdapter(context);
        }
        if (adapter == null || !adapter.isEnabled()) {
            if (BleConfig.isDebugEnabled()) {
                Log.e(TAG, "initialize: Unable to obtain a BluetoothAdapter.");
            }
            return false;
        }

        return true;
    }

    /**
     * Resets connection (if it was connected) with specified device.
     * @param address device address.
     */
    public void reset(String address) {
        reconnectionTries.remove(address);
        connectedDevices.remove(address);
        final BluetoothGatt gatt = gattMap.get(address);
        if (gatt != null) {
            gatt.disconnect();
            gatt.close();
            gattMap.remove(address);
        }
    }

    /**
     * Connects to the GATT server hosted on the Bluetooth LE device.
     *
     * @param address The device address of the destination device.
     *
     * @return Return true if the connection is initiated successfully. The connection result
     *         is reported asynchronously through the
     *         {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     *         callback.
     */
    public boolean connect(Context context, String address) {
        if (adapter == null || address == null) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "connect: BluetoothAdapter not initialized or unspecified address.");
            }
            return false;
        }

        // Previously connected device.  Try to reconnect.
        if (connectedDevices.contains(address) && gattMap.containsKey(address)) {
            if (BleConfig.isDebugEnabled()) {
                Log.d(TAG, "connect: Trying to use an existing BluetoothGatt for connection.");
            }
            if (gattMap.get(address).connect()) {
                connectionState = STATE_CONNECTING;
                return true;
            } else {
                return reconnect(context, address);
            }
        }

        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        final BluetoothDevice device = adapter.getRemoteDevice(address);
        final BluetoothGatt gatt = device.connectGatt(context, false, executor);
        if (gatt == null) {
            return reconnect(context, address);
        }
        if (BleConfig.isDebugEnabled()) {
            Log.d(TAG, "connect: Trying to create a new connection.");
        }
        gattMap.put(address, gatt);
        connectedDevices.add(address);
        connectionState = STATE_CONNECTING;
        return true;
    }

    private boolean reconnect(Context context, String address) {
        final int tries = reconnectionTries.get(address) + 1;
        reconnectionTries.put(address, tries);
        if (tries >= MAX_RECONNECTION_TRIES) {
            return false;
        }
        // try once more
        if (BleConfig.isDebugEnabled()) {
            Log.d(TAG, "reconnect: Trying to create connection again.");
        }
        return connect(context, address);
    }

    /**
     * Disconnects an existing connection or cancel a pending connection. The disconnection result
     * is reported asynchronously through the
     * {@code BluetoothGattCallback#onConnectionStateChange(android.bluetooth.BluetoothGatt, int, int)}
     * callback.
     */
    public void disconnect() {
        reconnectionTries.clear();
        connectedDevices.clear();
        for (BluetoothGatt gatt : gattMap.values()) {
            if (gatt != null) {
                gatt.disconnect();
            }
        }
    }

    /**
     * After using a given BLE device, the app must call this method to ensure resources are
     * released properly.
     */
    public void close() {
        reconnectionTries.clear();
        connectedDevices.clear();
        for (BluetoothGatt gatt : gattMap.values()) {
            if (gatt != null) {
                gatt.close();
            }
        }
        gattMap.clear();
    }

    /**
     * Retrieves a list of supported GATT services on the connected device. This should be
     * invoked only after {@code BluetoothGatt#discoverServices()} completes successfully.
     *
     * @return A {@code List} of supported services.
     */
    public List<BluetoothGattService> getSupportedGattServices(String address) {
        if (!gattMap.containsKey(address)) {
            return null;
        }

        return gattMap.get(address).getServices();
    }

    public void update(String address, Sensor<?> sensor, String uuid, Bundle data) {
        if (sensor == null) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "update: sensor not found");
            }
            return;
        }

        if (!gattMap.containsKey(address)) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "update: BluetoothAdapter not initialized");
            }
            return;
        }

        executor.update(sensor, uuid, data);
        if (isReady()) {
            executor.execute(gattMap.get(address));
        }
    }

    public void listen(String address, Sensor<?> sensor, String uuid) {
        if (sensor == null) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "listen: sensor not found");
            }
            return;
        }

        if (!gattMap.containsKey(address)) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "listen: BluetoothAdapter not initialized");
            }
            return;
        }

        executor.notify(sensor, uuid, true);
        if (isReady()) {
            executor.execute(gattMap.get(address));
        }
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.d
     */
    public void read(String address, Sensor<?> sensor, String uuid) {
        if (sensor == null) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "notify: sensor not found");
            }
            return;
        }

        if (!gattMap.containsKey(address)) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "read: BluetoothAdapter not initialized");
            }
            return;
        }

        executor.read(sensor, uuid);
        if (isReady()) {
            executor.execute(gattMap.get(address));
        }
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
        final String name = gatt.getDevice().getName();
        final String address = gatt.getDevice().getAddress();
        final Set<WeakReference<BleServiceListener>> listeners = cloneListeners();
        if (status != BluetoothGatt.GATT_SUCCESS) {
            for (WeakReference<BleServiceListener> r : listeners) {
                final BleServiceListener listener = r.get();
                if (listener != null) {
                    listener.onConnectionFailed(name, address, status, newState);
                }
            }
            return;
        }

        switch (newState) {
            case BluetoothProfile.STATE_CONNECTED:
                connectionState = STATE_CONNECTED;
                final boolean discoveryResult = gatt.discoverServices();
                if (BleConfig.isDebugEnabled()) {
                    Log.d(TAG, "state: Connected to GATT server.");
                    Log.d(TAG, "state: Attempting to start service discovery: " + discoveryResult);
                }
                for (WeakReference<BleServiceListener> r : listeners) {
                    final BleServiceListener listener = r.get();
                    if (listener != null) {
                        listener.onConnected(name, address);
                    }
                }
                break;
            case BluetoothProfile.STATE_DISCONNECTED:
                connectionState = STATE_DISCONNECTED;
                if (BleConfig.isDebugEnabled()) {
                    Log.d(TAG, "state: Disconnected from GATT server.");
                }
                for (WeakReference<BleServiceListener> r : listeners) {
                    final BleServiceListener listener = r.get();
                    if (listener != null) {
                        listener.onDisconnected(name, address);
                    }
                }
                break;
            case BluetoothProfile.STATE_CONNECTING:
                if (BleConfig.isDebugEnabled()) {
                    Log.d(TAG, "state: Connecting to GATT server.");
                }
                break;
            case BluetoothProfile.STATE_DISCONNECTING:
                if (BleConfig.isDebugEnabled()) {
                    Log.d(TAG, "state: Disconnecting from GATT server.");
                }
                break;
            default:
                if (BleConfig.isDebugEnabled()) {
                    Log.d(TAG, "state: Unknown status: " + status);
                }
        }
    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status) {
        if (BleConfig.isDebugEnabled()) {
            Log.d(TAG, "discovery: onServicesDiscovered received: " + status);
            final List<BluetoothGattService> services = gatt.getServices();
            Log.d(TAG, "discovery:                services found: " + services.size());
            for (BluetoothGattService service : services) {
                final List<BluetoothGattCharacteristic> characteristics =
                        service.getCharacteristics();
                Log.d(TAG, "discovery: " + service.getUuid() + " characteristics: " + characteristics.size());
                for (BluetoothGattCharacteristic characteristic : characteristics) {
                    Log.d(TAG, "discovery: \t" + characteristic.getUuid() + " "
                            + readableProperties(characteristic.getProperties()));
                }
            }
        }
        if (status == BluetoothGatt.GATT_SUCCESS) {
            connectionState = STATE_READY;
            final String name = gatt.getDevice().getName();
            final String address = gatt.getDevice().getAddress();
            final Set<WeakReference<BleServiceListener>> listeners = cloneListeners();
            for (WeakReference<BleServiceListener> r : listeners) {
                final BleServiceListener listener = r.get();
                if (listener != null) {
                    listener.onServiceDiscovered(name, address);
                }
            }
        }
    }

    private static String readableProperties(int properties) {
        final StringBuilder builder = new StringBuilder();
        if ((properties & BluetoothGattCharacteristic.PROPERTY_READ) != 0) {
            builder.append("READ");
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_WRITE) != 0) {
            if (builder.length() > 0) {
                builder.append(" | ");
            }
            builder.append("WRITE");
        }
        if ((properties & BluetoothGattCharacteristic.PROPERTY_NOTIFY) != 0) {
            if (builder.length() > 0) {
                builder.append(" | ");
            }
            builder.append("NOTIFY");
        }
        return builder.toString();
    }

    @Override
    public void onCharacteristicRead(BluetoothGatt gatt,
                                     BluetoothGattCharacteristic characteristic,
                                     int status) {
        if (status != BluetoothGatt.GATT_SUCCESS) {
            return;
        }

        final String name = gatt.getDevice().getName();
        final String address = gatt.getDevice().getAddress();
        final DeviceDef deviceDef = devices.get(name, address);
        if (deviceDef == null) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "Device definition missed: " + name + ":" + address);
            }
            return;
        }
        final Sensor<?> sensor = deviceDef.getSensor(
                characteristic.getService().getUuid().toString());
        if (sensor != null && !sensor.onCharacteristicRead(characteristic)) {
            return;
        }

        notifyUpdate(name, address, characteristic);
    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt,
                                        BluetoothGattCharacteristic characteristic) {

        final String name = gatt.getDevice().getName();
        final String address = gatt.getDevice().getAddress();
        final DeviceDef deviceDef = devices.get(name, address);
        if (deviceDef == null) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "Device definition missed: " + name + ":" + address);
            }
            return;
        }
        final Sensor<?> sensor = deviceDef.getSensor(
                characteristic.getService().getUuid().toString());
        if (sensor != null && !sensor.onCharacteristicChanged(characteristic)) {
            return;
        }
        notifyUpdate(name, address, characteristic);
    }

    private void notifyUpdate(String name, String address,
                              BluetoothGattCharacteristic characteristic) {
        final String serviceUuid = characteristic.getService().getUuid().toString();
        final String characteristicUuid = characteristic.getUuid().toString();

        final Set<WeakReference<BleServiceListener>> listeners = cloneListeners();
        for (WeakReference<BleServiceListener> r : listeners) {
            final BleServiceListener listener = r.get();
            if (listener != null) {
                listener.onCharacteristicChanged(name, address, serviceUuid, characteristicUuid);
            }
        }
    }

    private Set<WeakReference<BleServiceListener>> cloneListeners() {
        synchronized (serviceListeners) {
            return new HashSet<>(serviceListeners);
        }
    }

}
