/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/** BLE devices list scanner. */
public class BleScanner implements Runnable, BluetoothAdapter.LeScanCallback {
    /** Logging tag. */
    private static final String TAG = BleScanner.class.getSimpleName();

    /** Default scan period. */
    public static final long DEFAULT_SCAN_PERIOD = 5000L;
    /** Value of period used to scan for list of ble devices once. */
    public static final long PERIOD_SCAN_ONCE = -1;

    /** Bluetooth adapter. */
    private final BluetoothAdapter bluetoothAdapter;

    /** Scan period. */
    private long scanPeriod = DEFAULT_SCAN_PERIOD;
    /** Scan mode. */
    private int scanMode = ScanSettings.SCAN_MODE_LOW_LATENCY;
    /** Scan callback. */
    private ScanCallback scanCallback;

    /** Device filter. */
    private List<ScanFilter> filter = new ArrayList<>();
    /** Scan thread. */
    private Thread scanThread;
    /** Indicates whether scanner is running. */
    private final AtomicBoolean isScanning = new AtomicBoolean(false);

    /** UI/Main thread handler. */
    private final Handler mainThreadHandler = new Handler(Looper.getMainLooper());

    /** Scan process listener. */
    private BleScannerListener listener;

    public BleScanner(BluetoothAdapter adapter, BleScannerListener listener) {
        if (adapter == null) {
            throw new IllegalArgumentException("Adapter should not be null");
        }

        this.listener = listener;
        bluetoothAdapter = adapter;

        if (BleUtils.api21()) {
            scanCallback = new ScanCallback() {
                @TargetApi(Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    super.onScanResult(callbackType, result);
                    final byte[] bytes = result.getScanRecord() != null
                            ? result.getScanRecord().getBytes() : null;
                    if (BleConfig.isDebugEnabled()) {
                        Log.d(TAG, "Scan result: " + result);
                    }
                    onLeScan(result.getDevice(), result.getRssi(), bytes);
                }
            };
        }
    }

    /**
     * Sets scan period.
     * @param scanPeriod scan period for one scan cycle. Scanning will start automatically again on end of period.
     * To scan once only set {@link #PERIOD_SCAN_ONCE} as value.
     */
    public synchronized void setScanPeriod(long scanPeriod) {
        this.scanPeriod = scanPeriod < 0 ? PERIOD_SCAN_ONCE : scanPeriod;
    }

    /**
     * Sets scan mode. Scan will be restarted if was running.
     * @param scanMode scan mode to scan devices.
     */
    public void setScanMode(int scanMode) {
        final boolean restart = isScanning();
        this.scanMode = scanMode;
        if (restart) {
            stop();
            start();
        }
    }

    /**
     * Indicates whether scanning is in progress.
     * @return {@code true} if scanning is in progress.
     */
    public boolean isScanning() {
        return scanThread != null && scanThread.isAlive();
    }

    /**
     * Starts scanning for specified devices.
     * @param filters list of device names to search. <b>Please note that this parameter
     *                will be ignored for pre-Lollipop devices.</b>
     */
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public synchronized void start(@Nullable ScanFilter... filters) {
        if (isScanning()) {
            return;
        }

        filter.clear();
        if (filters != null) {
            Collections.addAll(filter, filters);
        }
        if (scanThread != null) {
            scanThread.interrupt();
        }
        scanThread = new Thread(this);
        scanThread.setName(TAG);
        scanThread.start();
    }

    /** Stops scanning. */
    public synchronized void stop() {
        isScanning.set(false);
        if (scanThread != null) {
            scanThread.interrupt();
            scanThread = null;
        }
        stopScanner();
    }

    private synchronized void startScanner() {
        if (BleUtils.api21()) {
            final ScanSettings settings = new ScanSettings.Builder()
                    .setScanMode(scanMode)
                    .build();
            final BluetoothLeScanner scanner = bluetoothAdapter.getBluetoothLeScanner();
            if (scanner != null) {
                scanner.startScan(filter, settings, scanCallback);
            }
        } else {
            //noinspection deprecation
            bluetoothAdapter.startLeScan(this);
        }
    }

    private synchronized void stopScanner() {
        if (BleUtils.api21()) {
            final BluetoothLeScanner scanner = bluetoothAdapter.getBluetoothLeScanner();
            if (scanner != null) {
                scanner.stopScan(scanCallback);
            }
        } else {
            //noinspection deprecation
            bluetoothAdapter.stopLeScan(this);
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void run() {
        try {
            isScanning.set(true);
            mainThreadHandler.post(new Runnable() {
                @Override
                public void run() {
                    listener.onScanStarted();
                }
            });

            do {
                startScanner();
                // wait until scan period finishes
                if (scanPeriod > 0) {
                    Thread.sleep(scanPeriod);
                } else {
                    Thread.sleep(DEFAULT_SCAN_PERIOD);
                }
                stopScanner();
                // repeat scan if needed
                if (scanPeriod > 0) {
                    mainThreadHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            listener.onScanRepeat();
                        }
                    });
                }
            } while (bluetoothAdapter.isEnabled() && isScanning.get() && scanPeriod > 0);
            //CHECKSTYLE:OFF
        } catch (InterruptedException ignore) {
            //CHECKSTYLE:ON
        } finally {
            stopScanner();
        }
        isScanning.set(false);
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                listener.onScanStopped();
            }
        });
    }

    @Override
    public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord) {
        mainThreadHandler.post(new Runnable() {
            @Override
            public void run() {
                listener.onLeScan(device, rssi, scanRecord);
            }
        });
    }

    /** Base implementation of BLE scan listener. */
    public static class BleScannerAdapter implements BleScannerListener {
        @Override
        public void onScanStarted() {
        }

        @Override
        public void onScanRepeat() {
        }

        @Override
        public void onScanStopped() {
        }

        @Override
        public void onLeScan(BluetoothDevice device, int i, byte[] bytes) {
        }
    }

    /** BLE devices scanner listener. */
    public interface BleScannerListener extends BluetoothAdapter.LeScanCallback {
        /** Notifies that scan process started. */
        void onScanStarted();
        /** Notifies that scan started next iteration. */
        void onScanRepeat();
        /** Notifies that scan process stopped.*/
        void onScanStopped();

        /** Notifies that device has been found. */
        @Override
        void onLeScan(BluetoothDevice device, int i, byte[] bytes);
    }

}
