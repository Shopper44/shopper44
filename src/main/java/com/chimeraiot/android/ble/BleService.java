/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

/**
 * Service for managing connection and data communication with a GATT server hosted on a
 * given Bluetooth LE device.
 */
public abstract class BleService extends Service implements BleServiceListener {
    /** Logging tag. */
    @SuppressWarnings("UnusedDeclaration")
    private static final String TAG = BleService.class.getSimpleName();

    /** Local binder instance. */
    private final IBinder binder = new LocalBinder();

    /** UI thread proxy. */
    private final ServiceConnectionProxy uiThreadProxy = new ServiceConnectionProxy();
    /** BLE manager. */
    private BleManager bleManager;

    @Override
    public void onCreate() {
        super.onCreate();
        bleManager = createBleManager();
        bleManager.registerListener(this);
    }

    /**
     * Creates BLE manager instance.
     * @return new BLE manager instance.
     */
    protected abstract BleManager createBleManager();

    @Override
    public IBinder onBind(Intent intent) {
        return binder;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        bleManager.unregisterListener(this);
        bleManager.disconnect();
        bleManager.close();
    }

    public BleManager getBleManager() {
        return bleManager;
    }

    public void setServiceListener(BleServiceListener listener) {
        uiThreadProxy.setServiceListener(listener);
    }

    @Override
    public void onConnected(String name, String address) {
        final Message msg = uiThreadProxy.get(ServiceConnectionProxy.MSG_CONNECTED,
                name, address);
        uiThreadProxy.sendMessage(msg);
    }

    @Override
    public void onConnectionFailed(String name, String address, int status, int state) {
        final Message msg = uiThreadProxy.get(ServiceConnectionProxy.MSG_CONNECTION_FAILED,
                name, address);
        msg.arg1 = status;
        msg.arg2 = state;
        uiThreadProxy.sendMessage(msg);
    }

    @Override
    public void onDisconnected(String name, String address) {
        final Message msg = uiThreadProxy.get(ServiceConnectionProxy.MSG_DISCONNECTED,
                name, address);
        uiThreadProxy.sendMessage(msg);
    }

    @Override
    public void onServiceDiscovered(String name, String address) {
        final Message msg = uiThreadProxy.get(ServiceConnectionProxy.MSG_SERVICE_DISCOVERED,
                name, address);
        uiThreadProxy.sendMessage(msg);
    }

    @Override
    public void onCharacteristicChanged(String name, String address, String serviceUuid,
                                        String characteristicUuid) {
        final Message msg = uiThreadProxy.get(ServiceConnectionProxy.MSG_CHARACTERISTIC_CHANGED,
                name, address);
        msg.getData().putString(ServiceConnectionProxy.ARG_SERVICE_UUID, serviceUuid);
        msg.getData().putString(ServiceConnectionProxy.ARG_CHARACTERISTICS_UUID, characteristicUuid);
        uiThreadProxy.sendMessage(msg);
    }

    /** Local binder. */
    public class LocalBinder extends Binder {

        public BleService getService() {
            return BleService.this;
        }

    }

    /** Proxy to notify UI thread about BLE connection. */
    private static class ServiceConnectionProxy extends Handler {
        /** Message to notify BLE connection established. */
        static final int MSG_CONNECTED = 0;
        /** Message to notify BLE connection failed. */
        static final int MSG_CONNECTION_FAILED = 1;
        /** Message to notify BLE disconnection. */
        static final int MSG_DISCONNECTED = 2;
        /** Message to notify service discovery completed. */
        static final int MSG_SERVICE_DISCOVERED = 3;
        /** Message to notify characteristic change. */
        static final int MSG_CHARACTERISTIC_CHANGED = 4;

        /** Device name. */
        static final String ARG_NAME = "device:name";
        /** Device address. */
        static final String ARG_ADDRESS = "device:address";

        /** Service UUID argument key. */
        static final String ARG_SERVICE_UUID = "uuid:service";
        /** Characteristics UUID argument key. */
        static final String ARG_CHARACTERISTICS_UUID = "uuid:characteristic";

        /** BLE connection listener. */
        private BleServiceListener serviceListener;

        ServiceConnectionProxy() {
            super(Looper.getMainLooper());
        }

        public void setServiceListener(BleServiceListener listener) {
            serviceListener = listener;
        }

        public Message get(int msgId, String name, String address) {
            final Message msg = obtainMessage(msgId);
            Bundle data = msg.getData();
            if (data == null) {
                data = new Bundle();
            } else {
                data.clear();
            }
            data.putString(ARG_NAME, name);
            data.putString(ARG_ADDRESS, address);
            msg.setData(data);
            return msg;
        }

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            if (serviceListener == null) {
                return;
            }

            final Bundle data = msg.getData();
            final String name = data.getString(ARG_NAME);
            final String address = data.getString(ARG_ADDRESS);
            switch (msg.what) {
                case MSG_CONNECTED:
                    serviceListener.onConnected(name, address);
                    break;
                case MSG_CONNECTION_FAILED:
                    serviceListener.onConnectionFailed(name, address, msg.arg1, msg.arg2);
                    break;
                case MSG_DISCONNECTED:
                    serviceListener.onDisconnected(name, address);
                    break;
                case MSG_SERVICE_DISCOVERED:
                    serviceListener.onServiceDiscovered(name, address);
                    break;
                case MSG_CHARACTERISTIC_CHANGED:
                    final String serviceUuid = msg.getData().getString(ARG_SERVICE_UUID);
                    final String characteristicUuid = msg.getData().getString(
                            ARG_CHARACTERISTICS_UUID);
                    serviceListener.onCharacteristicChanged(name, address, serviceUuid,
                            characteristicUuid);
                    break;
                default:
                    break;
            }
        }
    }
}
