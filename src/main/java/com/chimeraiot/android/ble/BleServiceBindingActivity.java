/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/** BLE service binding activity. */
public class BleServiceBindingActivity extends AppCompatActivity
                                       implements BleServiceListener,
                                                  ServiceConnection {
    /** Logging tag. */
    private static final String TAG = BleServiceBindingActivity.class.getSimpleName();

    /** Device name argument. */
    public static final String EXTRAS_DEVICE_NAME = TAG + ":DEVICE_NAME";
    /** Device address argument. */
    public static final String EXTRAS_DEVICE_ADDRESS = TAG + ":DEVICE_ADDRESS";

    /** Device name. */
    private String deviceName;
    /** Device address. */
    private String deviceAddress;
    /** BLE service. */
    private BleService bleService;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        final Intent intent = getIntent();
        deviceName = intent.getStringExtra(EXTRAS_DEVICE_NAME);
        deviceAddress = intent.getStringExtra(EXTRAS_DEVICE_ADDRESS);
    }

    @Override
    protected void onStart() {
        super.onStart();

        final Class serviceClass = getServiceClass();
        if (serviceClass == null) {
            throw new NullPointerException("Please specify service class");
        }

        final Intent gattServiceIntent = new Intent(this, serviceClass);
        bindService(gattServiceIntent, this, BIND_AUTO_CREATE);
    }

    @Override
    protected void onStop() {
        super.onStop();

        getBleManager().disconnect();
        getBleManager().close();
        unbindService(this);
    }

    public Class<? extends BleService> getServiceClass() {
        return null;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public String getDeviceAddress() {
        return deviceAddress;
    }

    public BleManager getBleManager() {
        return bleService.getBleManager();
    }

    @Override
    public void onConnected(String name, String address) {
    }

    @Override
    public void onConnectionFailed(String name, String address, int status, int state) {
        if (BleConfig.isDebugEnabled()) {
            Log.d(TAG, "connection changed :");
            Log.d(TAG, "            status : " + status);
            Log.d(TAG, "            state  : " + state);
        }
        getBleManager().connect(this, deviceAddress);
    }

    @Override
    public void onDisconnected(String name, String address) {
    }

    @Override
    public void onServiceDiscovered(String name, String address) {
    }

    @Override
    public void onCharacteristicChanged(String name, String address,
                                        String serviceUuid, String characteristicUuid) {
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        bleService = ((BleService.LocalBinder) service).getService();
        bleService.setServiceListener(this);
        //noinspection PointlessBooleanExpression,ConstantConditions
        if (!getBleManager().initialize(getBaseContext())) {
            if (BleConfig.isDebugEnabled()) {
                Log.w(TAG, "Unable to initialize Bluetooth");
            }
            finish();
            return;
        }

        // Automatically connects to the device upon successful start-up initialization.
        getBleManager().connect(this, deviceAddress);
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        bleService = null;
    }

    /**
     * Indicates whether BLE service connection established.
     * @return true is BLE service connection established
     */
    protected boolean isServiceConnected() {
        return bleService != null;
    }
}
