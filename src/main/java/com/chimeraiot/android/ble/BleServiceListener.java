/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble;

/** BLE service listener. */
public interface BleServiceListener {

    /**
     * Notifies about BLE device connected.
     * <b>This method is called on separate from Main thread.</b>
     */
    void onConnected(String name, String address);

    /**
     * Notifies about BLE device connection fail.
     * <b>This method is called on separate from Main thread.</b>
     */
    void onConnectionFailed(String name, String address, int status, int state);

    /**
     * Notifies about BLE device disconnected.
     * <b>This method is called on separate from Main thread.</b>
     */
    void onDisconnected(String name, String address);

    /**
     * Notifies about BLE device services discovered.
     * <b>This method is called on separate from Main thread.</b>
     */
    void onServiceDiscovered(String name, String address);

    /**
     * Notifies about BLE device characteristic changed.
     * <b>This method is called on separate from Main thread.</b>
     */
    void onCharacteristicChanged(String name, String address, String serviceUuid, String characteristicUuid);
}
