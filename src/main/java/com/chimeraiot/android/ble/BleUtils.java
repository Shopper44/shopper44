/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanFilter;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/** BLE utilities. */
public final class BleUtils {

    /** BLE enables status. */
    public static final int STATUS_BLE_ENABLED = 0;
    /** Bluetooth device is not available status. */
    public static final int STATUS_BLUETOOTH_NOT_AVAILABLE = 1;
    /** BLE is not supported status. */
    public static final int STATUS_BLE_NOT_AVAILABLE = 2;
    /** Bluetooth disabled status. */
    public static final int STATUS_BLUETOOTH_DISABLED = 3;

    /** BLE status. */
    @IntDef({
            STATUS_BLE_ENABLED,
            STATUS_BLUETOOTH_NOT_AVAILABLE,
            STATUS_BLE_NOT_AVAILABLE,
            STATUS_BLUETOOTH_DISABLED
    })
    @Retention(RetentionPolicy.SOURCE)
    public @interface BleStatus {
    }

    /** Data Mask. Used to prepare Service UUID. */
    private static final byte[] SCAN_FILTER_SERVICE_UUID_MASK = new byte[]{
            0,0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,0,0,0,0,0
    };

    /** Shows whether new BLE API avaialble. */
    public static boolean api21() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }

    /**
     * Prepares service UUID to be used as scan filter.
     * <br/>
     * As seen here: <a href="http://stackoverflow.com/a/30461322/322955">http://stackoverflow.com/a/30461322/322955</a>
     * @param uuid service uuid.
     * @return scan filter.
     */
    @Nullable
    public static ScanFilter getManufacturerDataFilter(String uuid) {
        if (!api21()) {
            return null;
        }
        final byte[] manData = new byte[]{0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        // Copy UUID into data array and remove all "-"
        System.arraycopy(hexStringToByteArray(uuid.replace("-","")), 0, manData, 2, 16);
        // Add data array to filters
        return new ScanFilter.Builder().setManufacturerData(76, manData,
                SCAN_FILTER_SERVICE_UUID_MASK).build();
    }

    /**
     * Returns bytes from hex string.
     * @param s hex string.
     * @return bytes representation of hex string.
     */
    private static byte[] hexStringToByteArray(String s) {
        final int len = s.length();
        final byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i+1), 16));
        }
        return data;
    }

    private BleUtils() {
        // prevent instantiation.
    }

    /**
     * Provides bluetooth adapter from system services.
     * @param context - used to get bluetooth system service.
     * @return bluetooth adapter.
     */
    @Nullable
    public static BluetoothAdapter getBluetoothAdapter(Context context) {
        final BluetoothManager bluetoothManager =
                (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        if (bluetoothManager == null) {
            return null;
        }
        return bluetoothManager.getAdapter();
    }

    /**
     * Return current BLE status.
     * @param context - used to get BLE device.
     * @return one of next statuses:
     * {@link #STATUS_BLE_ENABLED},
     * {@link #STATUS_BLE_NOT_AVAILABLE},
     * {@link #STATUS_BLUETOOTH_DISABLED},
     * {@link #STATUS_BLUETOOTH_NOT_AVAILABLE}
     */
    @BleStatus
    public static int getBleStatus(Context context) {
        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            return STATUS_BLE_NOT_AVAILABLE;
        }

        final BluetoothAdapter adapter = getBluetoothAdapter(context);
        // Checks if Bluetooth is supported on the device.
        if (adapter == null) {
            return STATUS_BLUETOOTH_NOT_AVAILABLE;
        }

        if (!adapter.isEnabled()) {
            return STATUS_BLUETOOTH_DISABLED;
        }

        return STATUS_BLE_ENABLED;
    }

    /**
     * Sets bluetooth enabled.
     * @param context used to get Bluetooth adapter.
     * @param enabled true to enable Bluetooth, false to disable.
     */
    public static void setBluetoothEnabled(Context context, boolean enabled) {
        final BluetoothAdapter bluetoothAdapter = getBluetoothAdapter(context);
        if (bluetoothAdapter != null) {
            if (enabled) {
                bluetoothAdapter.enable();
            } else {
                bluetoothAdapter.disable();
            }
        }
    }

    /**
     * Restarts Bluetooth.
     * @param context used to get Bluetooth adapter.
     */
    public static void restartBluetooth(final Context context) {
        final BluetoothAdapter bluetoothAdapter = getBluetoothAdapter(context);
        if (bluetoothAdapter == null) {
            return;
        }
        final boolean isEnabled = bluetoothAdapter.isEnabled();
        if (!isEnabled) {
            bluetoothAdapter.enable();
        } else {
            final BluetoothStateReceiver receiver = new BluetoothStateReceiver() {
                @Override
                public void onBluetoothStateChanged(final int state) {
                    if (state == BluetoothAdapter.STATE_OFF) {
                        bluetoothAdapter.enable();
                        unregister(context);
                    }
                }
            };
            receiver.register(context);
            bluetoothAdapter.disable();
        }
    }

    /** Generates BLE executor. */
    public static BleGattExecutor createExecutor(@NonNull final BleExecutorListener listener) {
        return new BleGattExecutor() {
            @Override
            public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                super.onConnectionStateChange(gatt, status, newState);
                listener.onConnectionStateChange(gatt, status, newState);
            }

            @Override
            public void onServicesDiscovered(BluetoothGatt gatt, int status) {
                super.onServicesDiscovered(gatt, status);
                listener.onServicesDiscovered(gatt, status);
            }

            @Override
            public void onCharacteristicRead(BluetoothGatt gatt,
                                             BluetoothGattCharacteristic characteristic,
                                             int status) {
                super.onCharacteristicRead(gatt, characteristic, status);
                listener.onCharacteristicRead(gatt, characteristic, status);
            }

            @Override
            public void onCharacteristicChanged(BluetoothGatt gatt,
                                                BluetoothGattCharacteristic characteristic) {
                super.onCharacteristicChanged(gatt, characteristic);
                listener.onCharacteristicChanged(gatt, characteristic);
            }
        };
    }
}
