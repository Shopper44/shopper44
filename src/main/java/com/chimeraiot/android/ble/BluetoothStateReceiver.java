package com.chimeraiot.android.ble;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

/** Bluetooth connection status broadcast. */
public class BluetoothStateReceiver extends BroadcastReceiver {
    /** Log tag. */
    private static final String TAG = BluetoothStateReceiver.class.getSimpleName();
    /** Indicates that broadcast receiver was registered and needs to be unregistered. */
    private boolean isRegistered = false;

    public void register(Context context) {
        final IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
        context.registerReceiver(this, intentFilter);
        isRegistered = true;
    }

    public void unregister(Context context) {
        if (isRegistered) {
            context.unregisterReceiver(this);
            isRegistered = false;
        }
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {
        final String action = intent.getAction();
        switch (action) {
            case BluetoothAdapter.ACTION_STATE_CHANGED:
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE,
                        BluetoothAdapter.STATE_OFF);
                if (BleConfig.isDebugEnabled()) {
                    final boolean enabled = state == BluetoothAdapter.STATE_ON;
                    Log.d(TAG, "bluetooth: " + enabled + " (state " + state + ")");
                }
                onBluetoothStateChanged(state);
                break;
            default:
                break;
        }
    }

    /**
     * Notifies about Bluetooth device state changed.
     * @param state new state.
     */
    public void onBluetoothStateChanged(int state) {
    }

}