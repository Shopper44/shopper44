/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble.sensor;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/** Device definition collection. */
public abstract class DeviceDefCollection {

    /** Supported devices. */
    private final Set<String> supportedDevices = new HashSet<>();

    /** Devices cache. */
    private final HashMap<String, DeviceDef> devicesMap = new HashMap<>();

    /**
     * Registers device.
     * @param name device name.
     */
    public void register(String name) {
        supportedDevices.add(name);
    }

    public Set<String> getSupportedDevices() {
        return Collections.unmodifiableSet(supportedDevices);
    }

    public boolean isSupported(String name) {
        return supportedDevices.contains(name);
    }

    /**
     * Provides device definition.
     * @param name - device name.
     * @param address - device address.
     * @return sensor.
     */
    public DeviceDef get(String name, String address) {
        final DeviceDef deviceDef;
        if (!devicesMap.containsKey(address)) {
            deviceDef = create(name, address);
            if (deviceDef == null) {
                return null;
            }
            devicesMap.put(address, deviceDef);
            return deviceDef;
        }
        //noinspection unchecked
        return devicesMap.get(address);
    }

    /**
     * Factory method to create device definition.
     * @param name - device name.
     * @param address - device address.
     * @return new device definition.
     */
    public abstract DeviceDef create(String name, String address);

}
