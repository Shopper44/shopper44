/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble.sensor;

import com.chimeraiot.android.ble.BleConfig;
import com.chimeraiot.android.ble.BleGattExecutor;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.os.Bundle;
import android.util.Log;

import java.util.UUID;

/**
 * Base sensor implementation`.
 * @param <T> - data type.
 */
public abstract class Sensor<T> {
    /** Logging tag. */
    @SuppressWarnings("UnusedDeclaration")
    private static final String TAG = Sensor.class.getSimpleName();

    /** Configugation UUID. */
    private static final String CHARACTERISTIC_CONFIG = "00002902-0000-1000-8000-00805f9b34fb";

    /** Data. */
    private T data;

    /**
     * Constructor.
     * @param data - instance of data.
     */
    protected Sensor(T data) {
        this.data = data;
    }

    /**
     * Writes new value of characteristic to remote service.
     * @param c - characteristic to be written.
     * @param data - data.
     * @return true if new value was applied.
     */
    protected abstract boolean apply(BluetoothGattCharacteristic c, T data);

    public abstract String getName();

    public T getData() {
        return data;
    }

    public String getCharacteristicName(String uuid) {
        return "Unknown";
    }

    public abstract String getServiceUUID();

    public boolean onCharacteristicChanged(BluetoothGattCharacteristic c) {
        return apply(c, data);
    }

    public boolean onCharacteristicRead(BluetoothGattCharacteristic c) {
        return apply(c, data);
    }

    public BleGattExecutor.ServiceAction[] update(String uuid, Bundle data) {
        return new BleGattExecutor.ServiceAction[] {
                BleGattExecutor.ServiceAction.VOID
        };
    }

    public BleGattExecutor.ServiceAction read(final String uuid) {
        return new BleGattExecutor.ServiceAction(
                uuid, BleGattExecutor.ServiceAction.ActionType.READ) {
            @Override
            public boolean execute(BluetoothGatt gatt) {
                final BluetoothGattCharacteristic characteristic = getCharacteristic(gatt, uuid);
                if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) == 0) {
                    if (BleConfig.isDebugEnabled()) {
                        Log.w(TAG, "Characteristic " + uuid + " can not be read");
                    }
                    return true;
                }

                boolean success = gatt.readCharacteristic(characteristic);
                if (!success) {
                    if (BleConfig.isDebugEnabled()) {
                        Log.w(TAG, "Read " + uuid + " FAILED!");
                    }
                }
                return false;
            }
        };
    }

    public BleGattExecutor.ServiceAction write(final String uuid, final byte[] value) {
        return new BleGattExecutor.ServiceAction(
                uuid, BleGattExecutor.ServiceAction.ActionType.WRITE) {
            @Override
            public boolean execute(BluetoothGatt gatt) {
                final BluetoothGattCharacteristic characteristic = getCharacteristic(gatt, uuid);
                if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_WRITE) == 0) {
                    if (BleConfig.isDebugEnabled()) {
                        Log.w(TAG, "Characteristic " + uuid + " can not be written");
                    }
                    return true;
                }

                characteristic.setValue(value);
                boolean success = gatt.writeCharacteristic(characteristic);
                if (!success) {
                    if (BleConfig.isDebugEnabled()) {
                        Log.w(TAG, "Write " + uuid + " FAILED!");
                    }
                }
                return false;
            }
        };
    }

    public BleGattExecutor.ServiceAction notify(final String uuid, final boolean start) {
        return new BleGattExecutor.ServiceAction(
                uuid, BleGattExecutor.ServiceAction.ActionType.NOTIFY) {
            @Override
            public boolean execute(BluetoothGatt gatt) {
                final UUID ccc = UUID.fromString(CHARACTERISTIC_CONFIG);

                final BluetoothGattCharacteristic characteristic = getCharacteristic(gatt, uuid);
                if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_NOTIFY) == 0) {
                    if (BleConfig.isDebugEnabled()) {
                        Log.w(TAG, "Characteristic " + uuid + " can not provide notifications");
                    }
                    return true;
                }

                final BluetoothGattDescriptor config = characteristic.getDescriptor(ccc);
                if (config == null) {
                    if (BleConfig.isDebugEnabled()) {
                        Log.w(TAG, "Characteristic " + uuid + " does not have notify descriptor");
                    }
                    return true;
                }

                // enable/disable locally
                gatt.setCharacteristicNotification(characteristic, start);
                // enable/disable remotely
                config.setValue(start ? BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE
                        : BluetoothGattDescriptor.DISABLE_NOTIFICATION_VALUE);
                boolean success = gatt.writeDescriptor(config);
                if (!success) {
                    if (BleConfig.isDebugEnabled()) {
                        Log.w(TAG, "Notify " + uuid + " FAILED!");
                    }
                }
                return false;
            }
        };
    }

    private BluetoothGattCharacteristic getCharacteristic(BluetoothGatt bluetoothGatt, String uuid) {
        final UUID serviceUuid = UUID.fromString(getServiceUUID());
        final UUID characteristicUuid = UUID.fromString(uuid);

        final BluetoothGattService service = bluetoothGatt.getService(serviceUuid);
        return service.getCharacteristic(characteristicUuid);
    }
}
