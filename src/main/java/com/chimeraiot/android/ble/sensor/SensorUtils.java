/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble.sensor;

import android.bluetooth.BluetoothGattCharacteristic;

import static android.bluetooth.BluetoothGattCharacteristic.FORMAT_SINT8;
import static android.bluetooth.BluetoothGattCharacteristic.FORMAT_UINT8;

/** Sensor utilities. */
public final class SensorUtils {

    /** Number of bits in byte. */
    private static final int BITS_IN_BYTE = 8;
    /** Unsigned integer mask. */
    private static final int UNSIGNED_MASK = 0xFF;

    private SensorUtils() {
        // prevent instantiation
    }

    /**
     * Gyroscope, Magnetometer, Barometer, IR temperature
     * all store 16 bit two's complement values in the awkward format
     * LSB MSB, which cannot be directly parsed as getIntValue(FORMAT_SINT16, offset)
     * because the bytes are stored in the "wrong" direction.
     *
     * This function extracts these 16 bit two's complement values.
     * */
    public static Integer int16(BluetoothGattCharacteristic c, int offset) {
        Integer lowerByte = c.getIntValue(FORMAT_UINT8, offset);
        if (lowerByte == null) {
            return 0;
        }
        Integer upperByte = c.getIntValue(FORMAT_SINT8, offset + 1); // Note: interpret MSB as signed.
        if (upperByte == null) {
            return 0;
        }

        return (upperByte << BITS_IN_BYTE) + lowerByte;
    }

    public static Integer uint16(BluetoothGattCharacteristic c, int offset) {
        Integer lowerByte = c.getIntValue(FORMAT_UINT8, offset);
        if (lowerByte == null) {
            return 0;
        }
        Integer upperByte = c.getIntValue(FORMAT_UINT8, offset + 1); // Note: interpret MSB as unsigned.
        if (upperByte == null) {
            return 0;
        }

        return (upperByte << BITS_IN_BYTE) + lowerByte;
    }

    public static int uint8(BluetoothGattCharacteristic c, int offset) {
        Integer value = c.getIntValue(FORMAT_UINT8, offset);
        if (value == null) {
            return 0;
        }
        return value;
    }

    public static byte toUint8(int value) {
        return (byte) (value & UNSIGNED_MASK);
    }

    public static byte[] toUint16(int value) {
        return new byte[] {
                toUint8(value),
                toUint8(value >> BITS_IN_BYTE),
        };
    }
}
