/*
 * Copyright (c) 2015 Chimera IoT
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.chimeraiot.android.ble.utils;

import java.lang.ref.WeakReference;
import java.util.Collections;
import java.util.HashMap;
import java.util.Set;

/** Weak set utilities. */
public final class WeakSetUtils {

    private WeakSetUtils() {
        // hide
    }

    public static <T> Set<WeakReference<T>> newWeakSet() {
        return Collections.newSetFromMap(new HashMap<WeakReference<T>, Boolean>());
    }

    public static <T> T remove(Set<WeakReference<T>> set, T item) {
        if (item == null) {
            return null;
        }
        T result = null;
        for (WeakReference<T> r : set) {
            final T l = r.get();
            if (item.equals(l)) {
                result = r.get();
                set.remove(r);
                break;
            }
        }
        return result;
    }

    public static <T> boolean add(Set<WeakReference<T>> set, T item) {
        for (WeakReference<T> r : set) {
            final T l = r.get();
            if (item.equals(l)) {
                return false;
            }
        }
        set.add(new WeakReference<>(item));
        return true;
    }

}
